#ifndef ESW_SERVER_UTILS_H
#define ESW_SERVER_UTILS_H

#include <string>
#include <vector>

class utils {
public:
    static int byteArrayToInt(const char *data);

    static void intToByteArray(int value, char *data);

    static void split(const std::string &txt, std::vector<std::string> &strings);
};


#endif //ESW_SERVER_UTILS_H
