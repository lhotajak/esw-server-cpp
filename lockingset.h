#ifndef ESW_SERVER_LOCKINGSET_H
#define ESW_SERVER_LOCKINGSET_H

#include <unordered_set>
#include <string>
#include <vector>

class LockingSet {
public:
    explicit LockingSet(int setSize);

    ~LockingSet();

    void addAll(std::vector<std::string> &vec);

    int getAndResetSize();

private:
    std::unordered_set<std::string> *set;
    pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
};


#endif //ESW_SERVER_LOCKINGSET_H
