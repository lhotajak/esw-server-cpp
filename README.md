### ESW HW08 - C++ Server
#### Jakub Lhoták

Assignment: https://cw.fel.cvut.cz/b212/courses/b4m36esw/labs/servers

### Prerequisites
- CMake 2.8+ ([link](https://cmake.org/))
- Protobuf ([link](https://github.com/protocolbuffers/protobuf/blob/main/src/README.md))

### How to compile & run
- compile Protobuf:
```
protoc --cpp_out=. esw_server.proto
```
- compile C++:
```
mkdir build
cd build
cmake ..
make
```
- run C++:
    - **PORT**: desired port (8080 default)
    - **SET_SIZE**: desired initial set size (6000000 default)
```
./esw_server [PORT [SET_SIZE]]
```

### Solution description
- non-blocking I/O server -> usage of **epoll** from HW03 ([link](https://cw.fel.cvut.cz/b212/courses/b4m36esw/labs/lab03))
- usage of **pthreads**
  - new thread for processing every **Request_PostWords** message
- using **std::unordered_set\<std::string\>** with **pthread mutex locks**