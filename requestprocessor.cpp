#include "requestprocessor.h"
#include "utils.h"
#include "gzip.h"
#include <pthread.h>

struct thread_args {
    const esw::Request *request;
    LockingSet *set;
    int fd;
};

void processGetCount(int size, int fd) {
    esw::Response response;
    response.set_counter(size);
    response.set_status(esw::Response_Status_OK);

    unsigned int responseSize = response.ByteSizeLong();
    char *outputData = new char[responseSize + 4];
    utils::intToByteArray(responseSize, outputData);
    response.SerializeToArray(outputData + 4, responseSize);
    long count_write = write(fd, outputData, responseSize + 4);
    if (count_write == -1) {
        throw std::runtime_error(std::string("write: ") + strerror(errno));
    }
    delete[] outputData;
}

void *processPostWords(void *args) {
    thread_args *th_args = ((thread_args *) args);
    const esw::Request *request = th_args->request;
    LockingSet *set = th_args->set;
    int fd = th_args->fd;

    std::string decompressed = Gzip::decompress(request->postwords().data());
    std::vector<std::string> wordList;
    utils::split(decompressed, wordList);
    set->addAll(wordList);

    // send response
    esw::Response response;
    response.set_status(esw::Response_Status_OK);

    unsigned int responseSize = response.ByteSizeLong();
    char *outputData = new char[responseSize + 4];
    utils::intToByteArray(responseSize, outputData);
    response.SerializeToArray(outputData + 4, responseSize);
    int count_write = write(fd, outputData, responseSize + 4);
    if (count_write == -1) {
        throw std::runtime_error(std::string("write: ") + std::strerror(errno));
    }
    delete[] outputData;
    delete th_args;
    delete request;
    return nullptr;
}

void processRequest(const esw::Request *request, LockingSet *set, int fd) {
    if (request->has_getcount()) { // GET COUNT -> answer immediately
        processGetCount(set->getAndResetSize(), fd);
        delete request;
    } else { // POST WORDS -> create new thread
        pthread_t threadId;
        thread_args *args = new thread_args();
        args->request = request;
        args->set = set;
        args->fd = fd;
        if (pthread_create(&threadId, NULL, processPostWords, (void *) args) != 0) {
            throw std::runtime_error(std::string("pthread_create: ") + std::strerror(errno));
        }
        if (pthread_detach(threadId) != 0) { // thread must be detached!
            throw std::runtime_error(std::string("pthread_detach: ") + std::strerror(errno));
        }
    }
}