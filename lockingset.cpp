#include "lockingset.h"

LockingSet::LockingSet(int setSize) {
    set = new std::unordered_set<std::string>(setSize);
}

void LockingSet::addAll(std::vector<std::string> &vec) {
    pthread_mutex_lock(&lock);
    set->insert(vec.begin(), vec.end());
    pthread_mutex_unlock(&lock);
}

int LockingSet::getAndResetSize() {
    pthread_mutex_lock(&lock);
    int size = set->size();
    set->clear();
    pthread_mutex_unlock(&lock);
    return size;
}

LockingSet::~LockingSet() = default;
