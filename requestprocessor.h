#ifndef ESW_SERVER_REQUESTPROCESSOR_H
#define ESW_SERVER_REQUESTPROCESSOR_H


#include "esw_server.pb.h"
#include "lockingset.h"

void processRequest(const esw::Request *request, LockingSet *set, int fd);

#endif //ESW_SERVER_REQUESTPROCESSOR_H
