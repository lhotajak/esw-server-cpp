#include "epolleswserverconnection.h"

#include <string>
#include <unistd.h>
#include <cstring>
#include "utils.h"
#include "esw_server.pb.h"
#include "requestprocessor.h"

using namespace std;


EpollESWServerConnection::EpollESWServerConnection(int cfd, EpollInstance &e, LockingSet *set) :
        EpollFd(cfd, e) {
    registerFd(EPOLLIN);
    this->set = set;
    buffer = (char *) malloc(bufferSize); // using malloc because of efficient reallocation
    if (buffer == nullptr) {
        throw std::runtime_error(std::string("malloc: could not allocate memory"));
    }
}

EpollESWServerConnection::~EpollESWServerConnection() {} // closing is manual -> do nothing here

void EpollESWServerConnection::handleEvent(uint32_t events) {
    // COMMUNICATION
    if ((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)) { // ERROR
        unregisterFd();
    } else {
        if (bufferPtrEnd + READ_SIZE > bufferSize) { // resize buffer if needed
            bufferSize *= 2;
            buffer = (char *) realloc(buffer, bufferSize);
            if (buffer == nullptr) {
                throw std::runtime_error(std::string("malloc: could not allocate memory"));
            }
        }
        long count = read(fd, buffer + bufferPtrEnd, READ_SIZE); // read from socket
        if (count == -1) {
            throw std::runtime_error(std::string("read: ") + std::strerror(errno));
        }
        bufferPtrEnd += count;

        if (count != 0) {
            processBuffer(); // process queue
        } else { // if nothing was read -> end of communication
            // Close
            unregisterFd();
            int close_ret = close(fd);
            if (close_ret == -1) {
                throw std::runtime_error(std::string("close: ") + std::strerror(errno));
            }
            free(buffer);
            delete this;
        }
    }
}

void EpollESWServerConnection::processBuffer() {
    do {
        long bytesToRead = bufferPtrEnd - bufferPtrStart;
        if (incomingMsgSize != -1 && bytesToRead >= incomingMsgSize) { // waiting for a message, enough data
            processMessage();
        } else if (incomingMsgSize == -1 && bytesToRead >= 4) { // enough bytes for reading incomingMsgSize
            incomingMsgSize = utils::byteArrayToInt(buffer + bufferPtrStart);
            bufferPtrStart += 4; // move pointer
        } else { // not enough data
            break;
        }
    } while (true);
}

void EpollESWServerConnection::processMessage() {
    esw::Request *request = new esw::Request;
    request->ParseFromArray(buffer + bufferPtrStart, incomingMsgSize);

    bufferPtrStart += incomingMsgSize; // move pointer
    incomingMsgSize = -1; // reset message size

    //  move buffer data (so we don't have to realloc so often
    int size = bufferPtrEnd - bufferPtrStart;
    for (int i = 0; i < size; ++i) {
        buffer[i] = buffer[i + bufferPtrStart];
    }
    bufferPtrStart = 0;
    bufferPtrEnd = size;
    processRequest(request, set, fd);
}