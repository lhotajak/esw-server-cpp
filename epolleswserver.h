#ifndef EPOLL_SERVER_EPOLLMYTCPSERVER_H
#define EPOLL_SERVER_EPOLLMYTCPSERVER_H

#include <sys/epoll.h>
#include "epollfd.h"
#include "epollinstance.h"
#include "lockingset.h"

class EpollESWServer : public EpollFd {
public:
    EpollESWServer(EpollInstance &e, int port, int setSize);

    ~EpollESWServer();

    void handleEvent(uint32_t events);

private:
    LockingSet *set;
};


#endif //EPOLL_SERVER_EPOLLMYTCPSERVER_H
