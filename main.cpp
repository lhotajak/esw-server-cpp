#include "epollinstance.h"
#include "epolleswserver.h"

#define DEFAULT_PORT 8080
#define DEFAULT_SET_SIZE 6000000

using namespace std;

int main(int argc, char *argv[]) {
    int port = DEFAULT_PORT;
    int setSize = DEFAULT_SET_SIZE;

    if (argc > 1) {
        port = stoi(argv[1]);
        if (argc > 2) {
            setSize = stoi(argv[2]);
        }
    }


    EpollInstance ep;
    EpollESWServer server(ep, port, setSize);

    for (;;) {
        ep.waitAndHandleEvents();
    }
}