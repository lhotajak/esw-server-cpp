#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <cstring>
#include <fcntl.h>
#include "epolleswserver.h"
#include "epolleswserverconnection.h"

EpollESWServer::EpollESWServer(EpollInstance &e, int port, int setSize) : EpollFd(-1, e) // -1 -> not set yet
{
    // init set
    set = new LockingSet(setSize);

    // init socket
    // create socket
    int sfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sfd == -1) {
        throw std::runtime_error(std::string("socket: ") + std::strerror(errno));
    }
    int option = 1;
    // set reusable
    int setsocket_ret = setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
    if (setsocket_ret == -1) {
        throw std::runtime_error(std::string("setsockopt: ") + std::strerror(errno));
    }

    // socket address + bind
    struct sockaddr_in saddr{};
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;              // IPv4
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);    // Bind to all available interfaces
    saddr.sin_port = htons(port);          // Requested port
    // assigning a name to a socket
    int ret = bind(sfd, (struct sockaddr *) &saddr, sizeof(saddr));
    if (ret == -1) {
        throw std::runtime_error(std::string("bind: ") + std::strerror(errno));
    }

    // socket should be non-blocking
    int flags = fcntl(sfd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(sfd, F_SETFL, flags);

    // create a server -> listen
    int ret_listen = listen(sfd, SOMAXCONN);
    if (ret_listen == -1) {
        throw std::runtime_error(std::string("listen: ") + std::strerror(errno));
    }

    // attach file descriptor
    fd = sfd;
    registerFd(EPOLLIN);

    std::cout << "Server started" << std::endl;
}

EpollESWServer::~EpollESWServer() {
    unregisterFd();
}

void EpollESWServer::handleEvent(uint32_t events) {
    // ACCEPT NEW CONNECTION
    if ((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)) { // ERROR
        unregisterFd();
    } else {
        int cfd = accept(fd, NULL, NULL);
        if (cfd == -1) {
            throw std::runtime_error(std::string("accept: ") + std::strerror(errno));
        }
        // Set the new file descriptor for non-blocking operations
        int flags_cfd = fcntl(cfd, F_GETFL, 0);
        flags_cfd |= O_NONBLOCK;
        fcntl(cfd, F_SETFL, flags_cfd);

        // Create new EpollESWServerConnection on heap (self deleting)
        new EpollESWServerConnection(cfd, this->epollInstance, set);
    }
}
