#ifndef EPOLL_SERVER_EPOLLMYTCPCONNECTION_H
#define EPOLL_SERVER_EPOLLMYTCPCONNECTION_H

#include <sys/epoll.h>
#include <queue>
#include "epollfd.h"
#include "epollinstance.h"
#include "esw_server.pb.h"
#include "lockingset.h"

#define READ_SIZE 4096

class EpollESWServerConnection : public EpollFd {
public:
    EpollESWServerConnection(int cfd, EpollInstance &e, LockingSet *set);

    ~EpollESWServerConnection();

    void handleEvent(uint32_t events);

private:
    int incomingMsgSize = -1;
    int bufferSize = READ_SIZE;
    int bufferPtrStart = 0;
    int bufferPtrEnd = 0;
    char *buffer;
    LockingSet *set;

    void processBuffer();

    void processMessage();
};


#endif //EPOLL_SERVER_EPOLLMYTCPCONNECTION_H
