#include "utils.h"

int utils::byteArrayToInt(const char *data) {
    return (unsigned int) ((unsigned char) (data[0]) << 24 |
                           (unsigned char) (data[1]) << 16 |
                           (unsigned char) (data[2]) << 8 |
                           (unsigned char) (data[3]));
}

void utils::intToByteArray(int value, char *data) {
    data[3] = value & 0x000000ff;
    data[2] = (value & 0x0000ff00) >> 8;
    data[1] = (value & 0x00ff0000) >> 16;
    data[0] = (value & 0xff000000) >> 24;
}

void utils::split(const std::string &txt, std::vector<std::string> &strings) {
    const char *s = txt.c_str();
    int size = txt.size();
    int start = 0;
    bool newWord = false;
    for (int i = 0; i < size; i++) {
        char c = s[i];
        if (!isspace(c)) {
            newWord = true;
            if (i + 1 < size && isspace(s[i + 1])) {
                strings.emplace_back(s + start, i + 1 - start);
                newWord = false;
            }
        } else {
            if (!newWord) {
                start = i + 1;
            }
        }
    }
    if (newWord) {
        strings.emplace_back(s + start, size - start);
    }
}